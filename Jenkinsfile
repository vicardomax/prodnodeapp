node {
    def app

    stage('Clone repository') {
        /* Cloning the Repository to our Workspace */

        checkout scm
    }

    stage('Build image') {
        /* This builds the actual image */

        app = docker.build("prodnodeapp")
    }

    stage('Test image') {

        app.inside {
            echo "Tests passed!"
        }
    }

    stage('Push image to Registry') {
        /*
			Pushing the docker image to Private Registry http://docker-registry.virt-dev.com
		*/
        def packageJSON = readJSON file: 'package.json'
        def packageJSONVersion = packageJSON.version /* Grab from package.json on the version of app to update docker image tag */
        docker.withRegistry('http://docker-registry.virt-dev.com') {
            app.push("${packageJSONVersion}-build-${env.BUILD_NUMBER}") /* this refers to build number in Jenkins and will be updated with tag in Docker Registry */
            app.push("latest")
            }
                echo "Trying to Push Docker Build to Docker Private Registry"
    }
    stage('Deploy App to Dev K8 Cluster') {
        /*
      Delete existing pods, then pushing the docker image to Dev K8 Cluster
      Still a static shell execution of helm chart Deployment
      Need to devise a helm chart repository to manage helm charts on-premise and reference to that
    */
        /* Taking note that the helm charts are sync via check scm in 1st stage and executed standalone in Jenkins workspace location
           Hence, all updates in helm charts must be done in the same prodnodeapp repo in order to execute here */
        sh '/usr/local/bin/helm --kubeconfig /opt/kube/config init --client-only'
        sh '/usr/local/bin/helm --kubeconfig /opt/kube/config delete prodnodeapp || exit 0'
        /* Command is to delete helm chart from local repo. Status in repo will show as deleted. Pods and service will be deleted in k8 cluster. exit 0 is to return status 0 (other +ve nos will fail shell step) so that job will continue*/
        sh '/usr/local/bin/helm --kubeconfig /opt/kube/config delete --purge prodnodeapp || exit 0'
        /* Command is to purge deleted helm charts from local helm repo. exit 0 is to return status 0 (other +ve nos will fail shell step) so that job will continue*/
        sh 'sleep 5' /* sleep for 20 sec to ensure that pods are cleared before new deployment */
        echo "Deploying app to Dev K8 Cluster"
        sh '/usr/local/bin/helm --kubeconfig /opt/kube/config install --name prodnodeapp prodnodeapp'
        /* no check mechanism here for deployment status. Need to insert some form of check to determine pod status else fail pipeline */
    }
}
